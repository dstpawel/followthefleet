﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FollowTheFleet.Migrations
{
    public partial class AddNewFieldOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Owner",
                table: "Cars",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Cars",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Owner",
                table: "Cars");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Cars");
        }
    }
}
