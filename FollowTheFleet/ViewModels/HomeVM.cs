﻿using FollowTheFleet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FollowTheFleet.ViewModels
{
    public class HomeVM
    {
        public string Name { get; set; }
        public List<Car> Cars { get; set; }
    }
}
