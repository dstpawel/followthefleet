﻿using EmailService;
using FollowTheFleet.Model;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FollowTheFleet.Services
{
    [DisallowConcurrentExecution]
    public class ServiceDateJob : IJob
    {
        private readonly ICarRepository _carRepository;
        private readonly IEmailSender _emailSender;
        public ServiceDateJob(ICarRepository carRepository, IEmailSender emailSender)
        {
            _carRepository = carRepository;
            _emailSender = emailSender;
        }

        public Task Execute(IJobExecutionContext context)
        {
            var cars = _carRepository.GetAllCars().Where(x => x.ServiceDate.Date < System.DateTime.Now.Date);
            
            if (cars.Any())
            {
                SendNotification(null, cars);
            }
            return Task.CompletedTask;
        }

        public void SendNotification(string user, IEnumerable<Car> cars)
        {
            var messageContent = string.Empty;
            foreach (var car in cars)
            {
                messageContent += "Model " + car.Brand + ". Data ostatniego serwisu: " + car.ServiceDate.ToString("dd-MM-yyyy") + Environment.NewLine;
            }

            var messageTitle = "Przeterminowane przeglądy";
            var messageBody = "Ilość samochodów po terminie przeglądu: " + cars.Count() + Environment.NewLine + messageContent; 

            var message = new Message(new string[] { "test@gmail.com" }, messageTitle, messageBody);

            _emailSender.SendEmail(message);
        }
    }
}
