﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FollowTheFleet.Model
{
   public interface ICarRepository
    {
        IEnumerable<Car> GetAllCars();
        Car GetSingleCar(int carId);

        void AddCar(Car car);
        void EditCar(Car car);
        void DeleteCar(Car car);
    }
}
