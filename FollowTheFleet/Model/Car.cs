﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FollowTheFleet.Model
{
    public class Car
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Brand { get; set; }
        //[RegularExpression(@"[A - Za - z]{1, 3}-[A-Za-z]{1,2}-[0-9]{1,4}$")]
        public string LicensePlate { get; set; }
        [Range(1900,2050)]
        public int YearOfProduction { get; set; }
        public int Mileage { get; set; }
        [DataType(DataType.Date)]
        public DateTime ServiceDate { get; set; }
        [StringLength(3)]
        public string Insurance { get; set; }
        public int Power { get; set; }
        public int Capacity { get; set; }
        public string FuelType { get; set; }
        public string Color { get; set; }
        public decimal Price { get; set; }
        [StringLength(200, ErrorMessage = "Field description length can't be more than 200 chars.")]
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Owner { get; set; }
        public List<Repair> Repairs { get; set; }

    }
}
