﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FollowTheFleet.Model
{
    public class Repair
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime FixedDate { get; set; }
        public bool IsFixed { get; set; }
    }
}
