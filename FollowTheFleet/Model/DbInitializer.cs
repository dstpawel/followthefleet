﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FollowTheFleet.Model
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            /*if (!context.Cars.Any())
            {
                context.AddRange(
                    new Car { Brand = "Ford", Capacity = 2000, Color = "Czerwony", FuelType = "Benzyna", Description = "Kierwoca Bartek bardzo go zajeżdza", Insurance = "TAK", LicensePlate = "NDZ25551", Mileage = 123000, PhotoUrl = "/images/fordMustang.jpg", ThumbnailUrl = "/images/fordMustang.jpg", ServiceDate = new DateTime(2019,3,20), Power = 200, YearOfProduction = 2005, Repairs = "W 2010 naprawiany rozrząd", Owner = "Bartek W"},
                    new Car { Brand = "Audi", Capacity = 3000, Color = "Czerwony", FuelType = "Benzyna", Description = "Kierwoca Bartek bardzo go zajeżdza", Insurance = "TAK", LicensePlate = "NDZ25551", Mileage = 123000, PhotoUrl = "/images/audiS5.jpg", ThumbnailUrl = "/images/audiS5.jpg", ServiceDate = new DateTime(2019, 3, 20), Power = 200, YearOfProduction = 2005, Repairs = "W 2010 naprawiany rozrząd", Owner = "Asia W" },
                    new Car { Brand = "BMW", Capacity = 1000, Color = "Czerwony", FuelType = "Benzyna", Description = "Kierwoca Bartek bardzo go zajeżdza", Insurance = "TAK", LicensePlate = "NDZ25551", Mileage = 123000, PhotoUrl = "/images/bmvx4.jpg", ThumbnailUrl = "/images/bmvx4.jpg", ServiceDate = new DateTime(2019, 3, 20), Power = 200, YearOfProduction = 2005, Repairs = "W 2010 naprawiany rozrząd", Owner = "Karol P" },
                    new Car { Brand = "Chevr", Capacity = 4000, Color = "Czerwony", FuelType = "Benzyna", Description = "Kierwoca Bartek bardzo go zajeżdza", Insurance = "TAK", LicensePlate = "NDZ25551", Mileage = 123000, PhotoUrl = "/images/chevroletCorvete.jpg", ThumbnailUrl = "/images/chevroletCorvete.jpg", ServiceDate = new DateTime(2019, 3, 20), Power = 200, YearOfProduction = 2005, Repairs = "W 2010 naprawiany rozrząd", Owner = "Patryk W" },
                    new Car { Brand = "Nissan", Capacity = 2000, Color = "Czerwony", FuelType = "Benzyna", Description = "Kierwoca Bartek bardzo go zajeżdza", Insurance = "TAK", LicensePlate = "NDZ25551", Mileage = 123000, PhotoUrl = "/images/nissan.jpg", ThumbnailUrl = "/images/nissan.jpg", ServiceDate = new DateTime(2019, 3, 20), Power = 200, YearOfProduction = 2005, Repairs = "W 2010 naprawiany rozrząd", Owner = "Czesiek Sz" },
                    new Car { Brand = "Jaguar", Capacity = 2000, Color = "Czerwony", FuelType = "Benzyna", Description = "Kierwoca Bartek bardzo go zajeżdza", Insurance = "TAK", LicensePlate = "NDZ25551", Mileage = 123000, PhotoUrl = "/images/jaguar.jpg", ThumbnailUrl = "/images/jaguar.jpg", ServiceDate = new DateTime(2019, 3, 20), Power = 200, YearOfProduction = 2005, Repairs = "W 2010 naprawiany rozrząd", Owner = "Olgierd P" }               
                    );
            }*/
            context.SaveChanges();
        }
    }
}
