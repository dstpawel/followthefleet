﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FollowTheFleet.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FollowTheFleet.Controllers
{
    [RequireHttps]
    public class CarController : Controller
    {
        private readonly ICarRepository _carRepository;
        private IHostingEnvironment _env;

        public CarController(ICarRepository carRepository, IHostingEnvironment env)
        {
            _carRepository = carRepository;
            _env = env;
        }

        public IActionResult Details(int id)
        {
            var car = _carRepository.GetSingleCar(id);
            if (car == null)
                return NotFound();
            return View(car);
        }

        public IActionResult Create(string FileName)
        {
            if (!string.IsNullOrEmpty(FileName))
                ViewBag.ImgPath = "/Images/" + FileName;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Car car)
        {
            if (ModelState.IsValid)
            {
                _carRepository.AddCar(car);
                return RedirectToAction(nameof(Index));
            }
            return View(car);
        }

        public IActionResult Edit(int id, string FileName)
        {
            var car = _carRepository.GetSingleCar(id);
            if (car == null)
                return NotFound();

            if (!string.IsNullOrEmpty(FileName))
                ViewBag.ImgPath = "/Images/" + FileName;
            else
                ViewBag.ImgPath = car.PhotoUrl;

            return View(car);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Car car)
        {
            if (ModelState.IsValid)
            {
                _carRepository.EditCar(car);
                return RedirectToAction(nameof(Index));
            }
            return View(car);
        }

        public IActionResult Delete(int id)
        {
            var car = _carRepository.GetSingleCar(id);
            if (car == null)
                return NotFound();
            return View(car);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id, string photoUrl)
        {
            var car = _carRepository.GetSingleCar(id);
            _carRepository.DeleteCar(car);

            if(photoUrl != null)
            {
                var webRoot = _env.WebRootPath;
                var filePath = Path.Combine(webRoot.ToString() + photoUrl);

                System.IO.File.Delete(filePath);
            }


            return RedirectToAction(nameof(Index));
        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile(IFormCollection form)
        {
            var webRoot = _env.WebRootPath;
            var filePath = Path.Combine(webRoot.ToString() + "\\images\\" + form.Files[0].FileName);

            if (form.Files[0].FileName.Length > 0)
            {
                using(var stream = new FileStream(filePath, FileMode.Create))
                {
                    await form.Files[0].CopyToAsync(stream);
                }
            }
            if (Convert.ToString(form[""]) == string.Empty || Convert.ToString(form[""]) == "0")
                return RedirectToAction(nameof(Create), new { FileName = Convert.ToString(form.Files[0].FileName) });
            return RedirectToAction(nameof(Edit), new { FileName = Convert.ToString(form.Files[0].FileName), id = Convert.ToString(form["Id"]) });
        }

        public async Task<IActionResult> Index(string searchString)
        {
            var cars = from m in _carRepository.GetAllCars()
                         select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                cars = cars.Where(s => s.Brand.Contains(searchString));
            }

            return View(await Task.FromResult(cars.ToList()));
        }

        [HttpPost]
        public string Index(string searchString, bool notUsed)
        {
            return "From [HttpPost]Index: filter on " + searchString;
        }
    }
}
