﻿using FollowTheFleet.Model;
using FollowTheFleet.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace FollowTheFleet.Controllers
{
    [RequireHttps]
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ICarRepository _carRepository;
        public HomeController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public IActionResult Index()
        {
            var cars = _carRepository.GetAllCars().OrderBy(s => s.Brand);
            var homeVM = new HomeVM()
            {
                Name = "Follow The Fleet",
                Cars = cars.ToList()
            };

            return View(homeVM);
        }

        public IActionResult Details(int id)
        {
            var car = _carRepository.GetSingleCar(id);
            if(car == null)
            {
                return NotFound();
            }
            return View(car);
        }
    }
}